
FROM php:5.6-fpm-alpine

RUN apk update

RUN apk add imagemagick git patch

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN install-php-extensions bcmath gd imagick intl mysqli soap zip

# Configuration commune

RUN touch /usr/local/etc/php/conf.d/common.ini \
    && echo -e "[Date]\ndate.timezone = Europe/Paris" >> /usr/local/etc/php/conf.d/common.ini

# Composer

RUN cd /usr/local/bin \
    && wget https://getcomposer.org/installer \
    && mv installer composer-setup.php \
    && php composer-setup.php \
    && rm composer-setup.php \
    && mv composer.phar composer